A ROS package to control a copter so that it follows ArUco patterns with the bottom camera.


Quick setup:

`catkin_make`

`source devel/setup.bash ##Or setup.zsh`

`##If no roscore is already running`

`roscore`

`##Run the projet's packet`

`rosrun ros_erle_pattern_follower ros_erle_pattern_follower`