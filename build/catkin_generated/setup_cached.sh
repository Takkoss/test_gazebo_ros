#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/takos/rendu/gazebo_test/devel:/home/takos/mybot_ws/devel:/opt/ros/melodic"
export LD_LIBRARY_PATH="/home/takos/mybot_ws/devel/lib:/opt/ros/melodic/lib"
export PWD="/home/takos/rendu/gazebo_test/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/takos/rendu/gazebo_test/devel/share/common-lisp:/home/takos/mybot_ws/devel/share/common-lisp"
export ROS_PACKAGE_PATH="/home/takos/rendu/gazebo_test/src:/home/takos/mybot_ws/src/mybot_control:/home/takos/mybot_ws/src/mybot_description:/home/takos/mybot_ws/src/mybot_gazebo:/opt/ros/melodic/share"