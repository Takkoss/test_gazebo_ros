# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/takos/rendu/gazebo_test/src/ros_erle_pattern_follower/src/image_subscriber.cpp" "/home/takos/rendu/gazebo_test/build/ros_erle_pattern_follower/CMakeFiles/ros_erle_pattern_follower.dir/src/image_subscriber.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROSCONSOLE_BACKEND_LOG4CXX"
  "ROS_BUILD_SHARED_LIBS=1"
  "ROS_PACKAGE_NAME=\"ros_erle_pattern_follower\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/takos/rendu/gazebo_test/src/ros_erle_pattern_follower/include"
  "/opt/ros/melodic/include"
  "/opt/ros/melodic/share/xmlrpcpp/cmake/../../../include/xmlrpcpp"
  "/opt/ros/melodic/share/orocos_kdl/cmake/../../../include"
  "/usr/include/opencv"
  "/usr/include/eigen3"
  "/usr/local/include/opencv4"
  "/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
